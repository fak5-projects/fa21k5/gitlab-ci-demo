//
// Created by simon on 13.09.21.
//
#include <stdio.h>
#include <stdlib.h>

int add2(int a, int b);

int add3(int a, int b, int c);

int main(int argc, char **argv) {


    printf("Testing add2\n");

    int a = rand() % 64;
    int b = rand() % 64;

    if (add2(a, b) == a + b) {
        printf("Test add2 successful\n");
    } else {
        printf("Test add2 failed. Did not get expected value.\n");
        return EXIT_FAILURE;
    }

    printf("Testing add3\n");

    int c = rand() % 64;
    if (add3(a, b, c) == a + b + c) {
        printf("Test add3 successful\n");
    } else {
        printf("Test add3 failed. Did not get expected value.\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int add2(int a, int b) {
    return a + b;
}

int add3(int a, int b, int c) {
    return a + b + c;
}
